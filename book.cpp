#include "Book.h"

Book& Book::operator = (const Book& obj) 
{
	author = obj.author;
	name = obj.name;
	pages = obj.pages;
	price = obj.price;

	return *this;
}

std::ostream& operator << (std::ostream& stream, const Book& obj) 
{
	stream << obj.name;

	return stream;
}

std::istream& operator >> (std::istream& stream, Book& obj) 
{
	stream >> obj.name >> obj.author >> obj.pages >> obj.price;

	return stream;
}

