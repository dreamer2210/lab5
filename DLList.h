#pragma once
#include <iostream>
#include "Book.h"

struct DLList
{
    struct Node
    {
        Book data;    //information
        Node* next;   //next node position
        Node* prev;   //previous node position
    };

    Node* F = NULL;   //first node
    Node* L = NULL;   //last node
    Node* C = NULL;   //current node
    int Count;        //node count

    void Out();
    void Info();
    void Clear();

    bool MoveNext();
    bool MovePrev();
    bool MoveFirst();
    bool MoveLast();

    bool Init(const Book&);
    bool AddNext(const Book&);
    bool AddPrev(const Book&);
    bool AddFirst(const Book&);
    bool AddLast(const Book&);

    bool Del(Book&);
    bool DelNext(Book&);
    bool DelPrev(Book&);
    bool DelFirst(Book&);
    bool DelLast(Book&);
};
