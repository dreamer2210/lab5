#include <iostream>
#include "DLList.h"
#include "Book.h"

using namespace std;

static DLList list;

void add();
bool addPos(int k, const Book& obj);

void remove();
bool removePos(int k);

void sort();
bool movePos(int k, DLList::Node* back);

int main(int argc, char* argv[])
{
	Book object;
	int number = 1;

	while(number > 0)
	{
		cout << "Enter number of task (0 for exit)" << endl;

		cout << "1: Add object" << endl;
		cout << "2: Delete object" << endl;
		cout << "3: Sort" << endl;
		cout << "4: Info" << endl;

		cin >> number;

		switch(number)
		{
			case 0:
				return 0;
			case 1:
				add();
				break;
			case 2:
				remove();
				break;
			case 3:
				sort();
				break;
			case 4:
				list.Info();
				break;
		}
	}

	return 0;
}

void add()
{
	Book object;
	int number = 1;

	cout << "Example: " << endl << endl;

	cout << "name: " << endl;
	cout << "author: " << endl;
	cout << "pages: " << endl;
	cout << "price: " << endl;

	cin >> object;

	while(number > 0)
	{
		cout << "1: First" << endl;
		cout << "2: Last" << endl;
		cout << "3: In any position" << endl;
		cout << "0: <---BACK" << endl;

		cin >> number;

		switch(number)
		{
			case 0:
				return;
			case 1:
				if(!list.AddFirst(object))
				{
					list.AddNext(object);

					cout << "=====Object added=====" << endl;
				}
				return;
			case 2:
				if(!list.AddLast(object))
				{
					list.AddNext(object);

					cout << "=====Object added=====" << endl;
				}
				return;
			case 3:
				int k;
				cout << "Enter position: ";
				cin >> k;
				if(addPos(k, object))
				{
					cout << "=====Object added=====" << endl;
				}
				else
				{
					cout << "=====ERROR=====" << endl;
				}
				return;
		}
	}
}

bool addPos(int k, const Book& obj)
{
	DLList::Node* back = list.C;

	if(movePos(k, back))
	{
		list.AddNext(obj);
		list.C = back;

		return true;
	}
		
 	return false;
}

void remove()
{
	Book object;
	int number = 1;

	while(number > 0)
	{
		cout << "1: First" << endl;
		cout << "2: Last" << endl;
		cout << "3: Remove from any position" << endl;
		cout << "0: <---BACK" << endl;

		cin >> number;

		switch(number)
		{
			case 0:
				return;
			case 1:
				if(list.DelFirst(object))
				{
					cout << "=====Object removed=====" << endl;
				}
				else
				{
					cout << "=====ERROR=====" << endl;
				}
				break;
			case 2:
				if(list.DelLast(object))
				{
					cout << "=====Object removed=====" << endl;
				}
				else
				{
					cout << "=====ERROR=====" << endl;
				}
				break;
			case 3:
				int k;
				cout << "Enter position: ";
				cin >> k;
				if(removePos(k))
				{
					cout << "=====Object removed=====" << endl;
				}
				else
				{
					cout << "=====ERROR=====" << endl;
				}
				break;
		}
	}
}

bool removePos(int k)
{
	DLList::Node* back = list.C;

	if(movePos(k, list.C))
	{
		list.Del(list.C->data);
		list.C = back;

		return true;
	}

	return false;
}

void sort()
{

}

bool movePos(int k, DLList::Node* back)
{
	back = list.C;

	list.C = list.F;

	for(size_t i = 0; i < k - 1; ++i)
	{
		if(!list.C->next)
		{
			return false;
		}

		list.C = list.C->next;
	}

 	return true;
}
